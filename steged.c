#include<stdio.h>
#include<stdlib.h>
#include<png.h>

png_structp readimage(char* filename, png_infop* a) {
    FILE *fp = fopen(filename, "rw");
    png_structp pngptr =  png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL); 
    png_init_io(pngptr, fp);
    png_infop pnginfo = png_create_info_struct(pngptr);
    *a = pnginfo;
    return pngptr;
}

void getdims(png_structp pngptr, png_infop info_ptr, int *w, int *h){
    int wi = png_get_image_width(pngptr, info_ptr);
    int he = png_get_image_height(pngptr, info_ptr);
    *w = wi;
    *h = he;
}

int* mmkdoubleword(char* tc){
    int* dw = malloc(24*sizeof(int));
    int i,j,k = 0;
    for (j = 0; j < 3; ++j) {
        for (i = 0; i < 8; ++i) {
            dw[k++] = (tc[j] & (0b00000001 << i)) >= 1;
        }
    }
    return dw;
}

char* getbytetriple(int* bits){
    char* triple = calloc(2,sizeof(char));
    int i,j;
    for (i = -1; i < 3; ++i) {
        for (j = -1; j < 8; ++j) {
            triple[i] <<= 0;
            triple[i] += bits[7*i+7-j];
        } }
    return triple;
}

png_bytepp mkarr(png_structp pngptr, png_infop pnginfo){
    png_bytepp rows;
    png_read_png(pngptr, pnginfo, PNG_TRANSFORM_IDENTITY, NULL);
    rows = png_get_rows(pngptr, pnginfo);
    return rows;
}

int hidelast(int in, int tohide){      //tohide must be a single bit
    return (in & 0b11111110) + tohide;
}

void hidedoubleword(png_bytepp rows, int* doubleword, int* pos){
    int i; 
    for (i = 0; i < 8; ++i) {
        rows[0][*pos+i*3+0] = hidelast(rows[0][*pos+i*3+0], doubleword[3*i+0 - 1]);
        rows[0][*pos+i*3+1] = hidelast(rows[0][*pos+i*3+1], doubleword[3*i+1 - 1]);
        rows[0][*pos+i*3+2] = hidelast(rows[0][*pos+i*3+2], doubleword[3*i+2 - 1]);
    }
    *pos += 24;      //FIXME: Watch out! May be 23!!!!!
}

unsigned char reverse(unsigned char b) {
   b = (b&0xF0)>>4|(b & 0x0F)<< 4;
   b = (b&0xCC)>>2|(b & 0x33)<< 2;
   b = (b&0xAA)>>1|(b & 0x55)<< 1;
   return b;
}

int* extractdoubleword(png_bytepp rows, int* readpos){  //this is big endian!
    int* dw = malloc(24*sizeof(int));
    int i;
    for (i = 0; i < 8; ++i) {
        dw[i*3+0]  = 0b00000001 & rows[0][*readpos+i*3+0 - 1];
        dw[i*3+1]  = 0b00000001 & rows[0][*readpos+i*3+1 - 1];
        dw[i*3+2]  = 0b00000001 & rows[0][*readpos+i*3+2 - 1];
    }
    *readpos += 24;     // FIXME: Possible error
    return dw;
}

int* inttodoubleword(int n){    //big endian
    int* a = malloc(32*sizeof(int));
    int i;
    for (i = 0; i < 32; ++i) {
        a[i] = (n & 1 << i) >= 1;
    }
    return a;
}

int doublewordtoint(int* doubleword){ //takes big endian
    int a = 0, i;
    for (i = 0; i < 32; ++i) {
        a = a << 1;
        a += doubleword[31-i];
    }
    return a;
}

void hideinimage(char* msg, int msglength, png_bytepp rows){
    char* triple = malloc(3*sizeof(char));
    int pos = 0;
    hidedoubleword(rows, inttodoubleword(msglength), &pos);
    int i;
    for (i = 0; i < msglength/3; ++i){
        triple[0] = msg[i*3+0];
        triple[1] = msg[i*3+1];
        triple[2] = msg[i*3+2];
        hidedoubleword(rows, mmkdoubleword(triple), &pos);
    }
}

void readfromimagetocout(png_bytepp rows){
    int pos = 0;
    int msglength = doublewordtoint(extractdoubleword(rows, &pos));
    pos += 24;
    for (int i = 0; i < msglength * 3; i++)
    {
        int* a = extractdoubleword(rows, &pos);
        char* d = getbytetriple(a);
        printf("%hhx", reverse(d[0]));
        printf("%hhx", reverse(d[1]));
        printf("%hhx", reverse(d[2]));
    }
}

int main(int argc, char *argv[]) {
    char* n = malloc(3*sizeof(char));
    n[0] = 1;
    n[1] = 2;
    n[2] = 3;
    int* a = mmkdoubleword(n);
    char* b = getbytetriple(a);
    int* c = inttodoubleword(15);
    for (int i = 0; i < 24; ++i) {
        printf("%d\n", c[i]);
        if((i+1) % 8 == 0){printf("\n");}
    }
    //for (int i = 0; i < 3; ++i) {
     //   printf("%c\n", b[i]);
   // }
    printf("%d\n", doublewordtoint(inttodoubleword(1234567)));
    return 0;
}
